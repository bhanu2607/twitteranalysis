# TwitterAnalysis

The project is about analysis of particular word or statement in twitter tweets in recent times as a Positive or negative to Twitter Users.


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bhanu2607/twitteranalysis.git
git branch -M main
git push -uf origin main
```

## The code separated into two branches frontend and backend.
[Frontend](https://gitlab.com/bhanu2607/twitteranalysis/-/tree/frontend) \
[Backend](https://gitlab.com/bhanu2607/twitteranalysis/-/tree/backend)
```
frontend - Angular JS, Plotly plots been used.
backend - Django Framework, DjangoRestFramework
```


## Below are the credential for testing
```
username : admin12
password : rest@12345
```
## Deploy
The project is deployed on the Heroku following is the link,
https://twitter-frontend-ana.herokuapp.com/
***
